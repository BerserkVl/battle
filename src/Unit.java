import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;

public class Unit {
    private Shape unitShape;
    private Image unitImage;
    private ImageView unitImgView;
    private int type;
    private int owner;
    private int x;
    private int y;
    private boolean isVisible;
    private Color currentColor;


    Unit(boolean isShape, int type, int owner, int x, int y) {
        this.type = type;
        this.owner = owner;
        this.x = x;
        this.y = y;
        isVisible = owner == Game.getYourID();
        if (isShape) {
            unitShape = new Rectangle(Global.WIDTH, Global.HEIGHT);
            if (owner == Game.getYourID()) {
                currentColor = Global.COLORS[type];
            } else {
                /*currentColor = Global.COLORS[type];
                unitShape.setStroke(Color.INDIGO);
                unitShape.setStrokeWidth(2);*/
                currentColor = Global.HIDDEN;
            }
            unitShape.setFill(currentColor);
            unitShape.setOnMousePressed(this::mPressed);
            unitShape.setOnMouseReleased(this::mReleased);
            unitShape.setOnMouseEntered(mouseEvent -> mEntered());
            unitShape.setOnMouseExited(mouseEvent -> mExited());
        } else {
            //VP1=visible player 1;
            //HP1=hidden player 1;
            if (isVisible) {
                switch (type) {
                    case 0:
                        unitImage = new Image(owner == 0 ? "Units/Flag/flagR40.png" : "Units/Flag/flagB40.png");
                        break;
                    case 1:
                        unitImage = new Image(owner == 0 ? "Units/Spy/spyR40.png" : "Units/Spy/spyB40.png");
                        break;
                    case 2:
                        unitImage = new Image(owner == 0 ? "Units/Scout/scoutR40.png" : "Units/Scout/scoutB40.png");
                        break;
                    case 3:
                        unitImage = new Image(owner == 0 ? "Units/Miner/minerR40.png" : "Units/Miner/minerB40.png");
                        break;
                    case 4:
                        unitImage = new Image(owner == 0 ? "Units/Sergeant/sergeantR40.png" : "Units/Sergeant/sergeantB40.png");
                        break;
                    case 5:
                        unitImage = new Image(owner == 0 ? "Units/Lieutenant/lieutenantR40.png" : "Units/Lieutenant/lieutenantB40.png");
                        break;
                    case 6:
                        unitImage = new Image(owner == 0 ? "Units/Captain/captainR40.png" : "Units/Captain/captainB40.png");
                        break;
                    case 7:
                        unitImage = new Image(owner == 0 ? "Units/Major/majorR40.png" : "Units/Major/majorB40.png");
                        break;
                    case 8:
                        unitImage = new Image(owner == 0 ? "Units/Colonel/colonelR40.png" : "Units/Colonel/colonelB40.png");
                        break;
                    case 9:
                        unitImage = new Image(owner == 0 ? "Units/General/generalR40.png" : "Units/General/generalB40.png");
                        break;
                    case 10:
                        unitImage = new Image(owner == 0 ? "Units/Marshal/marshalR40.png" : "Units/Marshal/marshalB40.png");
                        break;
                    case 11:
                        unitImage = new Image(owner == 0 ? "Units/Mine/mineR40.png" : "Units/Mine/mineB40.png");
                        break;

                }
            } else {
                unitImage = new Image(Unit.class.getResource(owner == 0 ? "Units/Unknown/unknownR40.png" : "Units/Unknown/unknownB40.png").toString());
            }
            unitImgView = new ImageView(unitImage);
            unitImgView.setOnMousePressed(this::mPressed);
            unitImgView.setOnMouseReleased(this::mReleased);
            unitImgView.setOnMouseEntered(mouseEvent -> mEntered());
            unitImgView.setOnMouseExited(mouseEvent -> mExited());
        }

    }

    private void mPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (unitShape != null) {
                unitShape.setFill(currentColor.darker());
            }
        }
    }

    private void mReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (unitShape != null) {
                unitShape.setFill(currentColor);
            }
            Game.unitAction(this);
        }
    }

    private void mEntered() {
        Game.updateInfo(owner == Game.getYourID() || isVisible ? type : -1);
    }

    private void mExited() {
        Game.updateInfo("prv");
    }

    public void show() {
        if (unitShape != null) {
            isVisible = true;
            if (owner == Game.getYourID()) {
                Stop[] stops = new Stop[]{new Stop(0, currentColor), new Stop(1, Color.WHITE)};
                LinearGradient linearGradient = new LinearGradient(0.6, 0.6, 1, 1, true, CycleMethod.NO_CYCLE, stops);
                unitShape.setFill(linearGradient);
            } else {
                currentColor = Global.COLORS[type];
                unitShape.setStroke(Color.INDIGO);
                unitShape.strokeTypeProperty().set(StrokeType.INSIDE);
                unitShape.setStrokeWidth(2);
                unitShape.setFill(currentColor);
            }
        }
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public int getType() {
        return type;
    }

    public int getOwner() {
        return owner;
    }

    public Shape getShape() {
        return unitShape;
    }

    public ImageView getUnitImgView() {
        return unitImgView;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }
}
