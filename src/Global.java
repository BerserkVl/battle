import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

//class for some global values
public class Global {
    static final Color[] COLORS = {Color.RED, Color.YELLOW, Color.DARKCYAN, Color.FUCHSIA, Color.PURPLE, Color.SPRINGGREEN,
            Color.ORANGE, Color.PINK, Color.AQUA, Color.VIOLET, Color.SALMON, Color.BROWN};
    static final Color HIDDEN = Color.GREY.darker();
    static final String[] VERTICAL = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    static final String[] HORIZONTAL = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
    static final String[] FIGURE_NAMES = {"Флаг", "Шпион", "Разведчик", "Сапёр", "Сержант", "Лейтенант", "Капитан", "Майор", "Полковник",
            "Генерал", "Маршал", "Мина"};
    static final int[] QUANTITIES = {1, 1, 8, 5, 4, 4, 4, 3, 2, 1, 1, 6};
    static final int HEIGHT = 40;
    static final int WIDTH = 40;
    static final int GAP = 10;
    static final int[] DX = {-1, 1, 0, 0};
    static final int[] DY = {0, 0, 1, -1};


    static int winner = 0;

    static int serverWaiting = 1;
    static int[] moves = new int[2];
    static StringBuilder matchInfo = new StringBuilder();


    public static void init() {

        matchInfo = new StringBuilder();
    }


    static void addMove(int id) {
        moves[id]++;
    }

    public static void saveMatch() {
        matchInfo.append("1 ").append(moves[0]).append(" 2 ").append(moves[1]).append('\n');
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy MM dd'+'hh-mm-ss");
        File file = new File(formatForDateNow.format(new Date()) + ".txt");
        try {
            PrintWriter printWriter = new PrintWriter(file);
            System.out.println(printWriter.checkError());
            printWriter.println(matchInfo.toString());
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            System.out.println("Save failed. Reason is " + e);
        }

    }

    public static void saveMap(Unit[][] map) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if ((i == 4 || i == 5) && (j == 2 || j == 3 || j == 6 || j == 7)) {
                    matchInfo.append("-2 ");
                } else if (map[i][j] != null) {
                    matchInfo.append('(').append(map[i][j].getOwner()).append(") ");
                    matchInfo.append(map[i][j].getType()).append(' ');
                    matchInfo.append('[').append(i).append(' ').append(j).append("] ");
                } else {
                    matchInfo.append("-1 ");
                }

            }
            matchInfo.append('\n');
        }
        matchInfo.append('\n');
    }

    //-1 -move; 0 - bothDied; 1 - attacker won; 2 - defender won
    public static void saveMove(Unit[][] map, int type, int xS, int yS, int xE, int yE) {
        switch (type) {
            case -1:
                matchInfo.append('(').append(map[xS][yS].getOwner()).append(") ");
                matchInfo.append(map[xS][yS].getType()).append(' ');
                matchInfo.append('[').append(xS).append(' ').append(yS).append("] ");
                matchInfo.append("-> ").append(xE).append(" ").append(yE).append('\n');
                break;
            case 0:
                matchInfo.append('(').append(map[xS][yS].getOwner()).append(") ");
                matchInfo.append(map[xS][yS].getType()).append(' ');
                matchInfo.append('[').append(xS).append(' ').append(yS).append("] ");
                matchInfo.append("X ");
                matchInfo.append('(').append(map[xE][yE].getOwner()).append(") ");
                matchInfo.append(map[xE][yE].getType()).append(' ');
                matchInfo.append('[').append(xE).append(' ').append(yE).append("] ");
                matchInfo.append("-> ").append("=").append('\n');
                break;
            case 1:
                matchInfo.append('(').append(map[xS][yS].getOwner()).append(") ");
                matchInfo.append(map[xS][yS].getType()).append(' ');
                matchInfo.append('[').append(xS).append(' ').append(yS).append("] ");
                matchInfo.append("X ");
                matchInfo.append('(').append(map[xE][yE].getOwner()).append(") ");
                matchInfo.append(map[xE][yE].getType()).append(' ');
                matchInfo.append('[').append(xE).append(' ').append(yE).append("] ");
                matchInfo.append("-> ");
                matchInfo.append('(').append(map[xS][yS].getOwner()).append(") ");
                matchInfo.append(map[xS][yS].getType()).append(' ');
                matchInfo.append('[').append(xE).append(' ').append(yE).append("] ").append('\n');
                break;
            case 2:
                matchInfo.append('(').append(map[xS][yS].getOwner()).append(") ");
                matchInfo.append(map[xS][yS].getType()).append(' ');
                matchInfo.append('[').append(xS).append(' ').append(yS).append("] ");
                matchInfo.append("X ");
                matchInfo.append('(').append(map[xE][yE].getOwner()).append(") ");
                matchInfo.append(map[xE][yE].getType()).append(' ');
                matchInfo.append('[').append(xE).append(' ').append(yE).append("] ");
                matchInfo.append("-> ");
                matchInfo.append('(').append(map[xE][yE].getOwner()).append(") ");
                matchInfo.append(map[xE][yE].getType()).append(' ');
                matchInfo.append('[').append(xE).append(' ').append(yE).append("] ").append('\n');
                break;
        }
    }


}
