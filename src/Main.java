import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {
    static Stage mainStage;


    static void changeScene(String scene) {
        switch (scene) {
            case "menu":
                mainStage.setScene(Menu.menuScene());
                break;
            case "game":
                mainStage.setScene(Game.gameScene());
                break;
        }
    }

    @Override
    public void start(Stage primaryStage) {
        mainStage = primaryStage;
        primaryStage.setScene(Menu.menuScene());
        primaryStage.setWidth(1280);
        primaryStage.setHeight(720);
        primaryStage.setTitle("Battle");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

