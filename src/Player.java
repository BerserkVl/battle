import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.ArrayList;
import java.util.HashMap;

class Player {
    private int id;
    private boolean isReady;
    private boolean isBot;
    private IntegerProperty[] killedEnemyUnits;
    private HashMap<Unit, ArrayList<HolderXY>> ownUnits;


    Player(int id) {
        this.id = id;
        isReady = false;
        isBot=false;
        killedEnemyUnits = new IntegerProperty[12];
        for (int i = 0; i < 12; i++) {
            killedEnemyUnits[i] = new SimpleIntegerProperty(0);
        }
        ownUnits = new HashMap<>();
    }

    void addUnit(Unit unit) {
        ownUnits.put(unit, null);
    }

    void removeUnit(Unit unit) {
        ownUnits.remove(unit);
    }

    void addKilledEnemyUnit(int type) {
        killedEnemyUnits[type].set(killedEnemyUnits[type].get() + 1);
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public void setBot(boolean bot) {
        isBot = bot;
    }

    public int getId() {
        return id;
    }

    boolean isEmptyMoves(Unit unit) {
        return ownUnits.get(unit) == null;
    }

    public boolean isReady() {
        return isReady;
    }

    public boolean isBot() {
        return isBot;
    }

    boolean isMoveExist(Unit unit, HolderXY holderXY) {
        for (HolderXY xy:ownUnits.get(unit)
             ) {
            if(xy.getX()==holderXY.getX()&&xy.getY()==holderXY.getY()){
                return true;
            }
        }
        return false;
    }

    public IntegerProperty[] getKilledEnemyUnits() {
        return killedEnemyUnits;
    }

    public HashMap<Unit, ArrayList<HolderXY>> getOwnUnits() {
        return ownUnits;
    }
}
