import javafx.application.Platform;


import java.util.ArrayList;
import java.util.Random;

class AI extends Player {


    AI(int id) {
        super(id);
        setBot(true);
        if (Game.getGameMode() == 2 && getId() == 0) {
            Global.saveMap(Game.inGameUnits);
        }
    }

    void start() {
        generateBoard();
        enable();
        setReady(true);
    }

    void generateBoard() {
       Game.randomSetup(getId());

    }


    void rndMove() {
        Random rnd = new Random();
        ArrayList<Unit> units = new ArrayList<>();
        for (Unit tmpF : getOwnUnits().keySet()) {
            if (getOwnUnits().get(tmpF) != null) {
                units.add(tmpF);
            }
        }
        int i = rnd.nextInt(units.size());
        Unit unit = units.get(i);
        int size = getOwnUnits().get(unit).size();
        HolderXY xy = getOwnUnits().get(unit).get(rnd.nextInt(size));
        Platform.runLater(() -> Game.botAction(unit, xy.getX(), xy.getY()));


    }

    void enable() {

        new Thread(() -> {
            while (!Game.isEnd() && Main.mainStage.isShowing()) {
                // System.out.println("running");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Game.getCurrentPlayer() == getId() && Game.getGameState() == 1) {
                   /* try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    Game.updateMoves(getOwnUnits());
                    if (Game.getGameState() == 1) {
                        rndMove();
                    }
                    System.out.println("bot makes his move");
                }

            }
        }).start();
    }
}
