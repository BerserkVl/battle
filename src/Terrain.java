import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.Random;

public class Terrain {

    Shape terrainShape;
    Image terrainImage;
    ImageView terrainImgView;
    Color currentColor;
    boolean isWater; //or isAvailable
    int x;
    int y;


    Terrain(boolean isShape, int x, int y) {
        this.x = x;
        this.y = y;
        isWater = ((x == 4 || x == 5) && (y == 2 || y == 3 || y == 6 || y == 7));
        if (isShape) {
            terrainShape = new Rectangle(Global.WIDTH, Global.HEIGHT);
            currentColor = isWater ? Color.BLUE : Color.GREEN;
            terrainShape.setFill(currentColor);
            terrainShape.setOnMousePressed(this::mPressed);
            terrainShape.setOnMouseReleased(this::mReleased);
            terrainShape.setOnMouseEntered(mouseEvent -> mEntered());
            terrainShape.setOnMouseExited(mouseEvent -> mExited());
        } else {
            //"/ground.png"
            if (isWater) {
                terrainImage = new Image(Terrain.class.getResourceAsStream("/Terrain/Water/water40.png"));
            } else {
                int rnd = new Random().nextInt(6);
                String path="";
                switch (rnd) {
                    case 0:
                        path="/Terrain/BeechTree/beechTree40.png";
                        break;
                    case 1:
                        path="/Terrain/DeadWood/deadWood40.png";
                        break;
                    case 2:
                        path="/Terrain/FruitTree/fruitTree40.png";
                        break;
                    case 3:
                        path="/Terrain/Pines/pines40.png";
                        break;
                    case 4:
                        path="/Terrain/PineTree/pineTree40.png";
                        break;
                    case 5:
                        path="/Terrain/PineTree2V/pineTree2V40.png";
                        break;
                }
                terrainImage = new Image(Terrain.class.getResourceAsStream(path));
            }
            terrainImgView = new ImageView(terrainImage);
            terrainImgView.setOnMousePressed(this::mPressed);
            terrainImgView.setOnMouseReleased(this::mReleased);
            terrainImgView.setOnMouseEntered(mouseEvent -> mEntered());
            terrainImgView.setOnMouseExited(mouseEvent -> mExited());
        }
    }

    private void mPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (terrainShape != null) {
                terrainShape.setFill(currentColor.darker());
            }
        }
    }

    private void mReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (terrainShape != null) {
                terrainShape.setFill(currentColor);
            }
            Game.terrainAction(this.x, this.y);
        }
    }

    private void mEntered() {
        Game.updateInfo(this.x, this.y);
    }

    private void mExited() {
        Game.updateInfo("prv");
    }

    public ImageView getImgView() {
        return terrainImgView;
    }

    public Shape getShape() {
        return terrainShape;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }
}
