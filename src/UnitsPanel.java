import javafx.beans.property.IntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;


public class UnitsPanel {
    private Shape btnShape;
    private Image btnImage;
    private ImageView btnImageView;
    private Label unitNameLabel;
    private Label quantityLabel;
    private HBox hBox;
    private Color currentColor;
    private boolean isShape;
    private int type;


    UnitsPanel(boolean isShape, IntegerProperty quantity, int type, boolean isLeftSide) {
        this.type = type;
        this.isShape = isShape;
        if (isShape) {
            btnShape = new Circle(20);
            currentColor = Global.COLORS[type];
            btnShape.setFill(currentColor);
            btnShape.setOnMousePressed(this::mPressed);
            btnShape.setOnMouseReleased(this::mReleased);
        } else {
            switch (type) {
                case 0:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Flag/flagR40.png" : "Units/Flag/flagB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Flag/flagB40.png" : "Units/Flag/flagR40.png");
                    }
                    break;
                case 1:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Spy/spyR40.png" : "Units/Spy/spyB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Spy/spyB40.png" : "Units/Spy/spyR40.png");
                    }
                    break;
                case 2:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Scout/scoutR40.png" : "Units/Scout/scoutB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Scout/scoutB40.png" : "Units/Scout/scoutR40.png");
                    }
                    break;
                case 3:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Miner/minerR40.png" : "Units/Miner/minerB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Miner/minerB40.png" : "Units/Miner/minerR40.png");
                    }
                    break;
                case 4:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Sergeant/sergeantR40.png" : "Units/Sergeant/sergeantB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Sergeant/sergeantB40.png" : "Units/Sergeant/sergeantR40.png");
                    }
                    break;
                case 5:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Lieutenant/lieutenantR40.png" : "Units/Lieutenant/lieutenantB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Lieutenant/lieutenantB40.png" : "Units/Lieutenant/lieutenantR40.png");
                    }
                    break;
                case 6:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0? "Units/Captain/captainR40.png" : "Units/Captain/captainB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Captain/captainB40.png" : "Units/Captain/captainR40.png");
                    }
                    break;
                case 7:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Major/majorR40.png" : "Units/Major/majorB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Major/majorB40.png" : "Units/Major/majorR40.png");
                    }
                    break;
                case 8:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Colonel/colonelR40.png" : "Units/Colonel/colonelB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Colonel/colonelB40.png" : "Units/Colonel/colonelR40.png");
                    }
                    break;
                case 9:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/General/generalR40.png" : "Units/General/generalB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/General/generalB40.png" : "Units/General/generalR40.png");
                    }
                    break;
                case 10:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Marshal/marshalR40.png" : "Units/Marshal/marshalB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Marshal/marshalB40.png" : "Units/Marshal/marshalR40.png");
                    }
                    break;
                case 11:
                    if (isLeftSide) {
                        btnImage = new Image(Game.getYourID() == 0 ? "Units/Mine/mineR40.png" : "Units/Mine/mineB40.png");
                    } else {
                        btnImage = new Image(Game.getOpponentID() == 1 ? "Units/Mine/mineB40.png" : "Units/Mine/mineR40.png");
                    }
                    break;

            }
            btnImageView = new ImageView(btnImage);
            btnImageView.setOnMousePressed(this::mPressed);
            btnImageView.setOnMouseReleased(this::mReleased);
        }
        unitNameLabel = new Label(Global.FIGURE_NAMES[type]);
        unitNameLabel.setFont(new Font("Lucida Sans", 20));
        quantityLabel = new Label();
        quantityLabel.textProperty().bind(quantity.asString());
        quantityLabel.setFont(new Font("Lucida Sans", 20));
        if (isLeftSide) {
            hBox = new HBox(isShape ? btnShape : btnImageView, unitNameLabel, quantityLabel);
            hBox.setAlignment(Pos.CENTER_LEFT);
        } else {
            hBox = new HBox(quantityLabel, unitNameLabel, isShape ? btnShape : btnImageView);
            hBox.setAlignment(Pos.CENTER_RIGHT);
        }
        hBox.setSpacing(5);
    }


    private void mPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (isShape) {
                btnShape.setFill(currentColor.darker());
            }
        }
    }

    private void mReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (Integer.parseInt(quantityLabel.getText()) > 0) {
                Game.setSelectedUnit(this.type);
            }
            if (isShape) {
                btnShape.setFill(currentColor);
            }
        }
    }

    public Shape getShape() {
        return btnShape;
    }

    public Image getImage() {
        return btnImage;
    }


    public HBox getHBox() {
        return hBox;
    }

    public void setBind(IntegerProperty newValue) {
        quantityLabel.textProperty().bind(newValue.asString());
    }
}
