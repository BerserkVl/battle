import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

//class for network side
public class Network {
    private static ServerSocket serverSocket;
    private static Socket socket;
    private static BufferedReader bufferedReader;
    private static PrintWriter printWriter;
    static String msg;
    static int x, y, x2, y2;

    static void createConnection(boolean isHost, String hostName) {
        try {
            if (isHost) {
                serverSocket = new ServerSocket(45800);
                System.out.println(serverSocket.getInetAddress().getHostName());
                waitFor();

            } else {
                socket = new Socket(hostName, 45800);
                init(2);

            }

        } catch (IOException e) {
            System.out.println("createConnection " + e);
        }
    }

    static void init(int playerId) {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream(), true);
            readMsg();
            Game.setGameMode(0);
            Game.setYourID(playerId);
            Platform.runLater(() -> Main.changeScene("game"));
        } catch (IOException e) {
            System.out.println("initStreams " + e);
        }
    }


    static void waitFor() {
        new Thread(() -> {
            while (Global.serverWaiting != 1) {
                try {
                    socket = serverSocket.accept();
                    init(1);
                    break;
                } catch (IOException e) {
                    System.out.println("waitFor " + e);
                }
            }

        }).start();
    }

    static void readMsg() {
        new Thread(() -> {
            try {
                while ((msg = bufferedReader.readLine()) != null && !Game.isEnd() && Main.mainStage.isShowing()) {

                   // System.out.println("--> " + msg);
                    if (msg.startsWith("ADD")) {
                        x = Integer.parseInt(msg.split(" ")[1]);
                        y = Integer.parseInt(msg.split(" ")[2]);
                        x2 = Integer.parseInt(msg.split(" ")[3]);//type
                        y2 = Integer.parseInt(msg.split(" ")[4]);//playerID
                        Platform.runLater(() -> Game.addUnit(x, y, x2, y2));
                    } else if (msg.startsWith("MOVE")) {
                        x = Integer.parseInt(msg.split(" ")[1]);
                        y = Integer.parseInt(msg.split(" ")[2]);
                        x2 = Integer.parseInt(msg.split(" ")[3]);
                        y2 = Integer.parseInt(msg.split(" ")[4]);
                        Platform.runLater(() -> Game.humanAction(x, y, x2, y2));
                    } else if (msg.startsWith("NOT_READY")) {
                        Game.setOpponentReady(false);
                    } else if (msg.startsWith("READY")) {
                        Game.setOpponentReady(true);
                    }else if(msg.startsWith("START")){
                        Platform.runLater(Game::start);
                    }
                }
            } catch (IOException e) {
                System.out.println("readMsg " + e);
                closeSocket();
            }
        }).start();
    }

    static void printMsg(String msgType, int fd, int sd, int td, int frd) {
        printWriter.println(msgType + " " + fd + " " + sd + " " + td + " " + frd);
    }

    static void closeServerSocket() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            System.out.println("closeServerSocket " + e);
        }
    }

    static void closeSocket() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("closeSocket " + e);
            }
        }
    }

    static String getHostName() {
        String local = "";
        try {
            local = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            System.out.println("getHostName " + e);
        }
        return local;
    }

}
