import javafx.animation.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

//class for game
public class Game {

    static Scene scene;
    static Label topInfo;
    static Label bottomInfo;
    static Label mainMenu;
    static Label retry;
    static int selectedUnit;
    static Unit prvUnit;
    static BorderPane borderPane;
    static GridPane fieldPane;
    static GridPane backFieldPane;
    static GridPane indicatePane;
    static StackPane holder;
    static IntegerProperty[] unitQuantity;
    static Unit[][] inGameUnits;

    static Terrain[][] terrains;
    static Rectangle[][] backField;
    static Rectangle[][] indicate;

    static UnitsPanel[] unitsPanelsLeft;
    static UnitsPanel[] unitsPanelsRight;
    static Player[] players;
    static AI[] bots;

    static int currentPlayer = 0;
    static int yourID = 0;
    static int opponentID = 1;
    static int gameMode = 1; //0-PVP; 1-PVB; 2-BVB; (P-player, B-bot)
    static int gameState = 0; // 0 - set ; 1 - game; 2 - game over
    static volatile boolean isEnd = false;

    static VBox vBoxLeft;
    static VBox vBoxRight;
    static VBox vBoxInfo;

    static AudioClip turn = new AudioClip(Game.class.getResource("/sound/beep.wav").toString());

    static RotateTransition rotateFieldPane;
    static RotateTransition rotateBackFieldPane;
    static FillTransition[][] fillIndicate;

    static String prvInfo;

    static boolean isShape = false;


    /**
     * flag = index 0
     * spy = index 1
     * scout = index 2
     * miner = index 3
     * sergeant = index 4
     * lieutenant = index 5
     * captain = index 6
     * major = index 7
     * colonel = index 8
     * general = index 9
     * marshal = index 10
     * bomb = index 11
     */

    static Scene gameScene() {

        selectedUnit = -1;
        prvInfo = "";
        setUnitQuantity();
        createField();
        createBackField();
        inGameUnits = new Unit[10][10];
        playersInit();
        borderPane = new BorderPane();
        holder = new StackPane();
        holder.setMaxSize(600, 600);
        holder.getChildren().addAll(backFieldPane, indicatePane, fieldPane);

        topInfo = new Label("Press to ready");
        topInfo.setFont(new Font("Lucida Sans", 20));
        topInfo.setOnMousePressed(Game::topInfoMPressed);
        topInfo.setOnMouseReleased(Game::topInfoMReleased);

        mainMenu = new Label("Main Menu");
        mainMenu.setFont(new Font("Lucida Sans", 20));
        mainMenu.setOnMousePressed(Game::mainMenuMPressed);
        mainMenu.setOnMouseReleased(Game::mainMenuReleased);

        retry = new Label("Retry");
        retry.setFont(new Font("Lucida Sans", 20));
        retry.setOnMousePressed(Game::retryMPressed);
        retry.setOnMouseReleased(Game::retryMReleased);

        bottomInfo = new Label("Info panel");
        bottomInfo.setFont(new Font("Lucida Sans", 20));
        bottomInfo.setAlignment(Pos.BOTTOM_CENTER);

        borderPane.setCenter(holder);
        borderPane.setLeft(vBoxLeft);
        borderPane.setRight(vBoxRight);
        borderPane.setTop(topInfo);
        borderPane.setBottom(bottomInfo);
        borderPane.setPadding(new Insets(0, 130, 200, 130));
        BorderPane.setAlignment(bottomInfo, Pos.TOP_CENTER);
        BorderPane.setAlignment(topInfo, Pos.BOTTOM_CENTER);

        scene = new Scene(borderPane);
        borderPane.requestFocus();
        borderPane.setOnKeyPressed(Game::controlKPressed);
        borderPane.setOnKeyReleased(Game::controlKReleased);
        return scene;
    }


    static void topInfoMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            topInfo.setTextFill(Color.MAROON.brighter());
        }
    }

    static void topInfoMReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            topInfo.setTextFill(Color.MAROON);
            if (gameState == 0) {
                if (checkReady() && players[opponentID].isReady()) {
                    if (gameMode == 0) {
                        Network.printMsg("START", 0, 0, 0, 0);
                    }
                    start();
                } else if (players[yourID].isReady()) {
                    players[yourID].setReady(false);
                    topInfo.setText("Press to ready");
                    Network.printMsg("NOT_READY", 0, 0, 0, 0);
                } else if (checkReady()) {
                    players[yourID].setReady(true);
                    topInfo.setText("Waiting for opponent | Press to unready");
                    Network.printMsg("READY", 0, 0, 0, 0);
                }
            } else if (gameState == 2) {
                Main.changeScene("menu");
                Global.init();
            }
        }
    }

    static void mainMenuMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            mainMenu.setTextFill(Color.MAROON.brighter());
        }
    }

    static void mainMenuReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            mainMenu.setTextFill(Color.MAROON);
            Main.changeScene("menu");
        }
    }

    static void retryMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            retry.setTextFill(Color.MAROON.brighter());
        }
    }

    static void retryMReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            retry.setTextFill(Color.MAROON);
            Main.changeScene("game");
        }
    }

    static void controlKPressed(KeyEvent keyEvent) {
        if (gameState == 0) {
            switch (keyEvent.getCode()) {
                case DIGIT1:
                    break;
                case DIGIT2:
                    break;
                case DIGIT3:
                    break;
                case DIGIT4:
                    break;
                case DIGIT5:
                    break;
                case DIGIT6:
                    break;
                case Q:
                    break;
                case W:
                    break;
                case E:
                    break;
                case R:
                    break;
                case T:
                    break;
                case Y:
                    break;
                case SPACE:
                    break;
            }
        }
    }

    static void controlKReleased(KeyEvent keyEvent) {
        if (gameState == 0) {
            switch (keyEvent.getCode()) {
                case DIGIT1:
                    setSelectedUnit(0);
                    break;
                case DIGIT2:
                    setSelectedUnit(1);
                    break;
                case DIGIT3:
                    setSelectedUnit(2);
                    break;
                case DIGIT4:
                    setSelectedUnit(3);
                    break;
                case DIGIT5:
                    setSelectedUnit(4);
                    break;
                case DIGIT6:
                    setSelectedUnit(5);
                    break;
                case Q:
                    setSelectedUnit(6);
                    break;
                case W:
                    setSelectedUnit(7);
                    break;
                case E:
                    setSelectedUnit(8);
                    break;
                case R:
                    setSelectedUnit(9);
                    break;
                case T:
                    setSelectedUnit(10);
                    break;
                case Y:
                    setSelectedUnit(11);
                    break;
                case SPACE:
                    break;
                case ESCAPE:

                    break;
                case F1:
                    Global.saveMatch();
                    break;
                case F12:
                    randomSetup(yourID);
                    break;

            }
        }
    }

    static void createField() {
        fieldPane = new GridPane();
        fieldPane.setGridLinesVisible(false);
        fieldPane.setAlignment(Pos.CENTER);
        fieldPane.setHgap(Global.GAP);
        fieldPane.setVgap(Global.GAP);
        rotateFieldPane = new RotateTransition(Duration.seconds(3), fieldPane);
        terrains = new Terrain[10][10];

        indicate = new Rectangle[10][10];
        indicatePane = new GridPane();
        indicatePane.setGridLinesVisible(false);
        indicatePane.setAlignment(Pos.CENTER);
        indicatePane.setHgap(Global.GAP - 6);
        indicatePane.setVgap(Global.GAP - 6);

        fillIndicate = new FillTransition[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                terrains[i][j] = new Terrain(false, i, j);
                indicate[i][j] = new Rectangle(Global.WIDTH + 6, Global.HEIGHT + 6, Color.YELLOW);
                indicate[i][j].setVisible(false);
                indicatePane.add(indicate[i][j], j, i);
                fillIndicate[i][j] = new FillTransition(Duration.seconds(1), indicate[i][j], Color.RED, Color.PURPLE);
                fillIndicate[i][j].setCycleCount(Animation.INDEFINITE);
                fillIndicate[i][j].setAutoReverse(true);
                //to shape its should be getShape() instead of getImgView()
                fieldPane.add(isShape ? terrains[i][j].getShape() : terrains[i][j].getImgView(), j, i);
                GridPane.setHalignment(terrains[i][j].getImgView(), HPos.CENTER);
                GridPane.setValignment(terrains[i][j].getImgView(), VPos.CENTER);
                GridPane.setHalignment(indicate[i][j], HPos.CENTER);
                GridPane.setValignment(indicate[i][j], VPos.CENTER);
            }
        }
        if (yourID == 1 && (gameMode == 0 || gameMode == 1)) {
            fieldPane.rotateProperty().set(180);
        }
    }

    static Label symbol(int i, int j) {
        Label label = new Label();
        label.setFont(new Font(Global.HEIGHT - 20));
        label.setAlignment(Pos.CENTER);
        GridPane.setHalignment(label, HPos.CENTER);
        if ((i == 0 || i == 11) && (j >= 1 && j <= 10)) {
            label.setText(Global.HORIZONTAL[j - 1]);
            if (i == 0) {
                label.rotateProperty().set(180);
            }
        } else if ((j == 0 || j == 11) && (i >= 1 && i <= 10)) {
            label.setText(Global.VERTICAL[i - 1]);
            if (j == 11) {
                label.rotateProperty().set(180);
            }
        }
        return label;
    }

    static void createBackField() {
        backFieldPane = new GridPane();
        backField = new Rectangle[12][12];
        rotateFieldPane = new RotateTransition(Duration.seconds(3), backFieldPane);
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 12; j++) {
                backField[i][j] = new Rectangle(Global.WIDTH, Global.HEIGHT, Color.RED);
                backField[i][j].setVisible(false);
                backFieldPane.add(backField[i][j], j, i);
                GridPane.setHalignment(backField[i][j], HPos.CENTER);
                GridPane.setValignment(backField[i][j], VPos.CENTER);
                backFieldPane.add(symbol(i, j), j, i);
            }
        }
        if (yourID == 1) {
            backFieldPane.rotateProperty().set(180);
        }
        backFieldPane.setAlignment(Pos.CENTER);
        backFieldPane.setGridLinesVisible(false);
        backFieldPane.setHgap(Global.GAP);
        backFieldPane.setVgap(Global.GAP);
    }

    static void createUnitPanel(boolean isLeftSide, IntegerProperty[] quantities) {

        if (isLeftSide) {
            unitsPanelsLeft = new UnitsPanel[12];
            vBoxLeft = new VBox();
            vBoxLeft.setSpacing(5);
            vBoxLeft.setAlignment(Pos.CENTER_RIGHT);
        } else {
            unitsPanelsRight = new UnitsPanel[12];
            vBoxRight = new VBox();
            vBoxRight.setSpacing(5);
            vBoxRight.setAlignment(Pos.CENTER_LEFT);

        }
        for (int i = 0; i < 12; i++) {
            if (isLeftSide) {
                unitsPanelsLeft[i] = new UnitsPanel(isShape, quantities[i], i, true);
                vBoxLeft.getChildren().add(unitsPanelsLeft[i].getHBox());
            } else {
                unitsPanelsRight[i] = new UnitsPanel(isShape, quantities[i], i, false);
                vBoxRight.getChildren().add(unitsPanelsRight[i].getHBox());
            }
        }
    }

    static void setUnitQuantity() {
        unitQuantity = new IntegerProperty[12];
        for (int i = 0; i < 12; i++) {
            unitQuantity[i] = new SimpleIntegerProperty(Global.QUANTITIES[i]);
        }
    }

    static void changePlayer() {
        System.out.println("from " + currentPlayer + " to " + (currentPlayer ^ 1));
        Global.addMove(currentPlayer);
        Global.saveMap(inGameUnits);
        currentPlayer ^= 1;
        if (currentPlayer == yourID && gameMode < 2) {
            play();
        }
        updateMoves(players[yourID].getOwnUnits());
        updateMoves(players[opponentID].getOwnUnits());

    }

    // 0 - bothDied; 1 - attacker won; 2 - defender won
    static void updateTopInfo(int whoWon, int atkX, int atkY, int defX, int defY) {
        Global.saveMove(inGameUnits, whoWon, atkX, atkY, defX, defY);

        String attacker = Global.FIGURE_NAMES[inGameUnits[atkX][atkY].getType()];
        String defender = Global.FIGURE_NAMES[inGameUnits[defX][defY].getType()];

        String atY = Global.HORIZONTAL[atkY];
        String atX = Global.VERTICAL[atkX];
        String dY = Global.HORIZONTAL[defY];
        String dX = Global.VERTICAL[defX];
        if (gameMode < 2) {
            if (currentPlayer == yourID) {
                topInfo.setText("Ваш " + attacker + " [" + atY + " : " + atX + "]" + " атаковал вражеского "
                        + defender + " [" + dY + " : " + dX + "] " + (whoWon == 0 ? "оба юнита погибли" : whoWon == 1 ? "вражеский юнит погиб" : "ваш юнит погиб"));
            } else {
                topInfo.setText("Вражеский " + attacker + " [" + atX + " : " + atY + "]" + " атаковал вашего "
                        + defender + " [" + dY + " : " + dX + "] " + (whoWon == 0 ? "оба юнита погибли" : whoWon == 1 ? "ваш юнит погиб" : "вражеский юнит погиб"));
            }
        }
    }

    static void updateTopInfo(Unit unit, int x, int y) {
        Global.saveMove(inGameUnits, -1, unit.getX(), unit.getY(), x, y);
        if (gameState == 1) {
            String yF = Global.HORIZONTAL[unit.getY()];
            String xF = Global.VERTICAL[unit.getX()];
            String yD = Global.HORIZONTAL[y];
            String xD = Global.VERTICAL[x];
            String name;
            if (unit.getOwner() == yourID || unit.isVisible()) {
                name = Global.FIGURE_NAMES[unit.getType()];
            } else {
                name = "Неизвестный юнит";
            }

            if (unit.getOwner() == yourID) {
                topInfo.setText("Ваш " + name + " [" + yF + " : " + xF + "] " + "передвинулся на " + "[" + yD + " : " + xD + "]");
            } else {
                topInfo.setText("Вражеский " + name + " [" + yF + " : " + xF + "] " + "передвинулся на " + "[" + yD + " : " + xD + "]");
            }
        }
    }

    static void updateInfo(String op) {
        switch (op) {
            case "placed":
                bottomInfo.setText("Разместить фигуру : " + Global.FIGURE_NAMES[selectedUnit]);
                prvInfo = "";
                break;
            case "selected":
                bottomInfo.setText("Переместить фигуру : " + Global.FIGURE_NAMES[prvUnit.getType()]);
                prvInfo = "";
                break;
            case "clear":
                bottomInfo.setText("");
                prvInfo = "";
                break;
            case "prv":
                bottomInfo.setText(prvInfo);
                prvInfo = "";
                break;
        }
    }

    static void updateInfo(int type) {
        prvInfo = bottomInfo.getText();
        if (type != -1) {
            bottomInfo.setText(Global.FIGURE_NAMES[type]);
        } else {
            bottomInfo.setText("Неизвестный юнит");
        }
    }

    static void updateInfo(int x, int y) {
        prvInfo = bottomInfo.getText();
        if (selectedUnit != -1 || prvUnit != null) {
            if ((yourID == 0 && x > 5) || (yourID == 1 && x < 4)) {
                bottomInfo.setText(prvInfo + " в клетку [" + Global.VERTICAL[x] + " : " + Global.HORIZONTAL[yourID == 0 ? y : Math.abs(y - 9)] + "]");
            }
        } else {
            bottomInfo.setText(terrains[x][y].isWater ? "Вода" : "Земля");
        }
    }

    static void setSelectedUnit(int type) {
        if (unitQuantity[type].get() > 0) {
            selectedUnit = type;
            updateInfo("placed");
        }
    }

    static void start() {
        topInfo.setText("Go!");
        for (int i = 0; i < 12; i++) {
            unitsPanelsLeft[i].setBind(players[yourID].getKilledEnemyUnits()[i]);
        }
        gameState = 1;
        System.out.println(gameMode + " " + gameState);
        Global.saveMap(inGameUnits);
        updateMoves(players[currentPlayer].getOwnUnits());

    }

    static boolean checkReady() {
        boolean isReady = true;
        for (int i = 0; i < 12; i++) {
            if (unitQuantity[i].get() > 0) {
                isReady = false;
                break;
            }
        }
        return isReady;
    }


    static void playersInit() {
        players = new Player[2];
        bots = new AI[2];
        bots[yourID] = (gameMode == 2 ? new AI(yourID) : null);
        bots[opponentID] = (gameMode > 0 ? new AI(opponentID) : null);
        players[yourID] = (gameMode < 2 ? new Player(yourID) : bots[yourID]);
        players[opponentID] = (gameMode == 0 ? new Player(opponentID) : bots[opponentID]);
        if (gameMode > 0) {
            bots[opponentID].start();
            if (gameMode == 2) {
                bots[yourID].start();
            }
        }
        createUnitPanel(true, unitQuantity);
        createUnitPanel(false, players[opponentID].getKilledEnemyUnits());
    }

    //0-both died, 1-attacker win, 2-defender win
    static void fight(int atkX, int atkY, int defX, int defY) {
        int attackPower = inGameUnits[atkX][atkY].getType();
        int defenderPower = inGameUnits[defX][defY].getType();
        switch (matcher(attackPower, defenderPower)) {
            case 0:
                updateTopInfo(0, atkX, atkY, defX, defY);
                bothDied(atkX, atkY, defX, defY);
                break;
            case 1:
                updateTopInfo(1, atkX, atkY, defX, defY);
                oneDied(atkX, atkY, defX, defY, false);
                if (defenderPower == 0) {
                    win("Flag", currentPlayer);
                }
                break;
            case 2:
                updateTopInfo(2, atkX, atkY, defX, defY);
                oneDied(atkX, atkY, defX, defY, true);
                break;
        }
        prvUnit = null;
    }

    static void bothDied(int atkX, int atkY, int defX, int defY) {
        if (inGameUnits[atkX][atkY].getOwner() == yourID) {
            players[yourID].removeUnit(inGameUnits[atkX][atkY]);
            players[opponentID].removeUnit(inGameUnits[defX][defY]);
        } else {
            players[yourID].removeUnit(inGameUnits[defX][defY]);
            players[opponentID].removeUnit(inGameUnits[atkX][atkY]);
        }
        players[yourID].addKilledEnemyUnit(inGameUnits[atkX][atkY].getType());
        players[opponentID].addKilledEnemyUnit(inGameUnits[defX][defY].getType());
        if (isShape) {
            fieldPane.getChildren().removeAll(inGameUnits[atkX][atkY].getShape(), inGameUnits[defX][defY].getShape());
        } else {
            fieldPane.getChildren().removeAll(inGameUnits[atkX][atkY].getUnitImgView(), inGameUnits[defX][defY].getUnitImgView());
        }
        inGameUnits[atkX][atkY] = null;
        inGameUnits[defX][defY] = null;

    }

    static void oneDied(int atkX, int atkY, int defX, int defY, boolean isAttackerDied) {
        int xD, yD, xS, yS;
        //xD,yD- whoDied, xS,yS - whoSurvive
        xD = isAttackerDied ? atkX : defX;
        yD = isAttackerDied ? atkY : defY;
        xS = isAttackerDied ? defX : atkX;
        yS = isAttackerDied ? defY : atkY;
        if (inGameUnits[xD][yD].getOwner() == yourID) {
            players[yourID].removeUnit(inGameUnits[xD][yD]);
            players[opponentID].addKilledEnemyUnit(inGameUnits[xD][yD].getType());
        } else {
            players[yourID].addKilledEnemyUnit(inGameUnits[xD][yD].getType());
            players[opponentID].removeUnit(inGameUnits[xD][yD]);
        }
        fieldPane.getChildren().remove(isShape ? inGameUnits[xD][yD].getShape() : inGameUnits[xD][yD].getUnitImgView());
        inGameUnits[xD][yD] = null;
        inGameUnits[xS][yS].show();

        if (!isAttackerDied) {
            moveUnit(inGameUnits[xS][yS], xD, yD);
        }
    }


    //result 0-both died, 1-attacker win, 2-defender win
    static int matcher(int attackerPower, int defenderPower) {
        int result = 0;
        if (defenderPower == 0) {
            result = 1;
        } else if (defenderPower == 11) {
            if (attackerPower != 3) {
                result = 2;
            } else {
                result = 1;
            }
        } else if (defenderPower == 10 && attackerPower == 1) {
            result = 1;
        } else if (attackerPower > defenderPower) {
            result = 1;
        } else if (defenderPower > attackerPower) {
            result = 2;
        }
        return result;
    }

    static void win(String winReason, int winnerID) {
        isEnd = true;
        gameState = 2;
        Global.saveMatch();
        if (gameMode < 2) {
            boolean isYouWinner = winnerID == yourID;
            if (winReason.equalsIgnoreCase("Flag")) {
                topInfo.setText(topInfo.getText() + " | " + (isYouWinner ? "\nYou captured enemy flag | Press to main menu" : "\nYou lose your flag | Press to main menu"));
            } else if (winReason.equalsIgnoreCase("No moves")) {
                topInfo.setText((isYouWinner ? "You lose, you cant move | Press to main menu" : "You win, opponent cant move | Press to main menu"));
            } else if (winReason.equalsIgnoreCase("surrender")) {
                topInfo.setText(isYouWinner ? "Opponent surrendered | Press to main menu" : "You surrendered | Press to main menu");
            }
        } else if (gameMode == 2) {
            if (winReason.equalsIgnoreCase("Flag")) {
                topInfo.setText(winnerID + " WON");
            } else if (winReason.equalsIgnoreCase("No moves")) {
                topInfo.setText(winnerID + " lose");
            }
        }
    }

    static void randomSetup(int id) {
        ArrayList<Integer> unitsTypes = new ArrayList<>();
        ArrayList<HolderXY> cells = new ArrayList<>();
        Random rnd = new Random();
        for (int i = 0; i < Global.QUANTITIES.length; i++) {
            for (int j = 0; j < Global.QUANTITIES[i]; j++) {
                unitsTypes.add(i);
            }
        }
        //if cpu played as first [0;4) or as second [6;10) player
        for (int i = (id == 0 ? 6 : 0); i < (id == 0 ? 10 : 4); i++) {
            for (int j = 0; j < 10; j++) {
                cells.add(new HolderXY(i, j));

            }
        }
        // random generation on which cell(cI) place random type of unit(fI)
        while (unitsTypes.size() > 0) {
            int fI = rnd.nextInt(unitsTypes.size());
            int cI = rnd.nextInt(cells.size());
            //  System.out.println(fI+" = "+units.elementAt(fI)+" | "+cI+" = "+cells.elementAt(cI));
            int x = cells.get(cI).getX();
            int y = cells.get(cI).getY();
            Game.addUnit(x, y, unitsTypes.get(fI), id);
            // should be better realization in string below
            //getOwnUnits().put(Game.inGameUnits[x][y], null);
            /* i can random only cI and fI just iterate from 0 to unitsTypes.size()-1 (mb result will be same with
             current realization)*/
            unitsTypes.remove(fI);
            cells.remove(cI);
        }
    }

    static void addUnit(int x, int y, int type, int id) {
        if (id == yourID) {
            prvUnit = null;
        }
        if (inGameUnits[x][y] == null) {

            if (players[id].isBot()) {
                inGameUnits[x][y] = new Unit(isShape, type, id, x, y);
                fieldPane.add(isShape ? inGameUnits[x][y].getShape() : inGameUnits[x][y].getUnitImgView(), y, x);
                bots[id].addUnit(inGameUnits[x][y]);
            } else {
                inGameUnits[x][y] = new Unit(isShape, type, id, x, y);
                fieldPane.add(isShape ? inGameUnits[x][y].getShape() : inGameUnits[x][y].getUnitImgView(), y, x);
                players[id].addUnit(inGameUnits[x][y]);
            }
            if (id == yourID) {
                unitQuantity[type].set(unitQuantity[type].get() - 1);
            }
        }
    }

    static void unitAction(Unit unit) {
        if (gameState == 0) {
            if (unit.getOwner() == yourID) {
                if (prvUnit == null) {
                    prvUnit = unit;
                    selectedUnit = -1;
                    updateInfo("selected");

                } else if (prvUnit != unit) {
                    if (gameMode == 0) {
                        Network.printMsg("MOVE", unit.getX(), unit.getY(), prvUnit.getX(), prvUnit.getY());
                    }
                    swapUnit(unit);
                    updateInfo("clear");
                }
            }
        } else if (yourID == currentPlayer && gameState == 1) {
            if (unit.getOwner() == yourID && yourID == currentPlayer) {
                if (!players[yourID].isEmptyMoves(unit)) {
                    prvUnit = unit;
                }
            } else {
                if (prvUnit != null) {
                    if (players[yourID].isMoveExist(prvUnit, new HolderXY(unit.getX(), unit.getY()))) {
                        if (gameMode == 0) {
                            Network.printMsg("MOVE", prvUnit.getX(), prvUnit.getY(), unit.getX(), unit.getY());
                        }
                        fight(prvUnit.getX(), prvUnit.getY(), unit.getX(), unit.getY());
                        changePlayer();

                    }
                }
            }
        }
    }

    static void terrainAction(int x, int y) {
        if (gameState == 0) {
            if ((yourID == 0 && x > 5) || (yourID == 1 && x < 4)) {
                if (selectedUnit != -1 && unitQuantity[selectedUnit].get() > 0) {
                    if (gameMode == 0) {
                        Network.printMsg("ADD", x, y, selectedUnit, yourID);
                    }
                    addUnit(x, y, selectedUnit, yourID);
                    if (unitQuantity[selectedUnit].get() == 0) {
                        updateInfo("clear");
                        selectedUnit = -1;
                    }
                } else if (prvUnit != null) {
                    if (gameMode == 0) {
                        Network.printMsg("MOVE", prvUnit.getX(), prvUnit.getY(), x, y);
                    }
                    moveUnit(prvUnit, x, y);
                    updateInfo("clear");
                }
            }
        } else if (currentPlayer == yourID && isAvailable(x, y) && gameState == 1) {
            if (gameMode == 0) {
                Network.printMsg("MOVE", prvUnit.getX(), prvUnit.getY(), x, y);
            }
            System.out.println("Step do");
            updateTopInfo(prvUnit, x, y);
            moveUnit(prvUnit, x, y);
            changePlayer();
        }
        // System.out.println(x + " " + y);
    }

    //x,y - where unit should move
    static void botAction(Unit unit, int x, int y) {
        if (inGameUnits[x][y] != null) {
            fight(unit.getX(), unit.getY(), x, y);
        } else {
            updateTopInfo(unit, x, y);
            moveUnit(unit, x, y);
        }
        changePlayer();
    }

    //xA,yA - actionUnit, x,y - where unit should move
    static void humanAction(int xA, int yA, int x, int y) {
        if (inGameUnits[x][y] != null) {
            if (gameState == 0) {
                swap(xA, yA, x, y);
            } else {
                fight(xA, yA, x, y);
            }
        } else {
            updateTopInfo(inGameUnits[xA][yA], x, y);
            moveUnit(inGameUnits[xA][yA], x, y);
        }
        if (gameState == 1) {
            changePlayer();
        }
    }

    static void swapUnit(Unit unit) {
        fieldPane.getChildren().removeAll(isShape ? unit.getShape() : unit.getUnitImgView(), isShape ? prvUnit.getShape() : prvUnit.getUnitImgView());
        int xF = unit.getX();
        int yF = unit.getY();
        int xT = prvUnit.getX();
        int yT = prvUnit.getY();
        inGameUnits[xT][yT] = unit;
        inGameUnits[xT][yT].setXY(xT, yT);
        fieldPane.add(isShape ? inGameUnits[xT][yT].getShape() : inGameUnits[xT][yT].getUnitImgView(), yT, xT);
        inGameUnits[xF][yF] = prvUnit;
        inGameUnits[xF][yF].setXY(xF, yF);
        fieldPane.add(isShape ? inGameUnits[xF][yF].getShape() : inGameUnits[xF][yF].getUnitImgView(), yF, xF);
        prvUnit = null;
    }

    static void swap(int x, int y, int x2, int y2) {
        fieldPane.getChildren().removeAll(inGameUnits[x][y].getShape(), inGameUnits[x2][y2].getShape());
        Unit tmp = inGameUnits[x][y];
        inGameUnits[x][y] = inGameUnits[x2][y2];
        inGameUnits[x][y].setXY(x, y);
        fieldPane.add(inGameUnits[x][y].getShape(), y, x);
        inGameUnits[x2][y2] = tmp;
        inGameUnits[x2][y2].setXY(x2, y2);
        fieldPane.add(inGameUnits[x2][y2].getShape(), y2, x2);
    }

    static boolean cellChecker(int x, int y) {
        boolean good = false;
        if ((x >= 0 && 9 >= x) && (y >= 0 && 9 >= y)) {
            if (inGameUnits[x][y] == null && !terrains[x][y].isWater) {
                good = true;
            } else if (inGameUnits[x][y] != null) {
                if (inGameUnits[x][y].getOwner() != currentPlayer) {
                    good = true;
                }
            }
        }
        return good;
    }

    static void updateMoves(HashMap<Unit, ArrayList<HolderXY>> pm) {
        System.out.println("This moves is " + currentPlayer);
        ArrayList<HolderXY> tmpAL;
        HolderXY xy;
        boolean emptyMoves = true;
        int x, y, dx, dy;
        for (Unit tmp : pm.keySet()) {
            if (tmp.getType() > 0 && tmp.getType() < 11) {
                tmpAL = new ArrayList<>();
                x = tmp.getX();
                y = tmp.getY();
                for (int i = 0; i < 4; i++) {
                    dx = x + Global.DX[i];
                    dy = y + Global.DY[i];

                    if (cellChecker(dx, dy)) {
                        xy = new HolderXY(dx, dy);
                        tmpAL.add(xy);
                        // if its scout
                        if (tmp.getType() == 2) {
                            while (true) {
                                dx += Global.DX[i];
                                dy += Global.DY[i];
                                if (cellChecker(dx, dy)) {

                                    tmpAL.add(new HolderXY(dx, dy));
                                    if (inGameUnits[dx][dy] != null) {
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
                pm.replace(tmp, tmpAL.isEmpty() ? null : tmpAL);
                if (!tmpAL.isEmpty()) {
                    emptyMoves = false;
                }
                if (pm.get(tmp) != null) {
                    System.out.print(tmp.getType() + "[" + tmp.getX() + "," + tmp.getY() + "]" + pm.get(tmp).toString() + " | ");
                }

            }
        }
        if (emptyMoves) {
            win("No moves", currentPlayer);
        }
        System.out.println();
    }

    static boolean isAvailable(int x, int y) {
        if (prvUnit != null) {
            return prvUnit.getOwner() == currentPlayer && players[yourID].isMoveExist(prvUnit, new HolderXY(x, y));
        }
        return false;
    }

    static void moveUnit(Unit unit, int x, int y) {
        fieldPane.getChildren().remove(isShape ? unit.getShape() : unit.getUnitImgView());
        inGameUnits[unit.getX()][unit.getY()] = null;
        inGameUnits[x][y] = unit;
        inGameUnits[x][y].setXY(x, y);
        fieldPane.add(isShape ? inGameUnits[x][y].getShape() : inGameUnits[x][y].getUnitImgView(), y, x);
        prvUnit = null;
    }

    public static void play() {
        turn.play();
    }

    public static void setYourID(int yourID) {
        Game.yourID = yourID;
    }

    public static void setGameMode(int gameMode) {
        Game.gameMode = gameMode;
    }

    public static void setOpponentReady(boolean ready) {
        players[opponentID].setReady(ready);
    }

    public static int getGameMode() {
        return gameMode;
    }

    public static int getGameState() {
        return gameState;
    }

    public static int getCurrentPlayer() {
        return currentPlayer;
    }

    public static int getYourID() {
        return yourID;
    }

    public static int getOpponentID() {
        return opponentID;
    }

    public static boolean isEnd() {
        return isEnd;
    }
}