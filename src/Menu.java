import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


//class for creation game
public class Menu {
    static BorderPane borderPane;
    static Label botLabel;
    static Label networkLabel;
    static Label hostLabel;
    static Label connectLabel;
    static Label backLabel;
    static Label exitLabel;
    static Label infoLabel;
    static TextField hostNameTextField;
    static VBox vBox;
    static VBox lanVBox;
    static HBox connectHBox;
    static int pos;

    static Scene menuScene() {
        pos = 0;
        borderPane = new BorderPane();
        botLabel = new Label("Play with bot");
        botLabel.setFont((new Font("Lucida Sans", 20)));
        networkLabel = new Label("LAN");
        networkLabel.setFont((new Font("Lucida Sans", 20)));
        exitLabel = new Label("Exit");
        exitLabel.setFont((new Font("Lucida Sans", 20)));
        hostLabel = new Label("Host");
        hostLabel.setFont((new Font("Lucida Sans", 20)));
        connectLabel = new Label("Connect");
        connectLabel.setFont((new Font("Lucida Sans", 20)));
        backLabel = new Label("Back");
        backLabel.setFont((new Font("Lucida Sans", 20)));
        infoLabel = new Label("");
        infoLabel.setFont((new Font("Lucida Sans", 20)));
        hostNameTextField = new TextField();
        hostNameTextField.setPromptText("Host name");
        hostNameTextField.setFocusTraversable(false);
        vBox = new VBox(20, botLabel, networkLabel, exitLabel);
        connectHBox = new HBox(15, connectLabel, hostNameTextField);
        connectHBox.setAlignment(Pos.CENTER);
        lanVBox = new VBox(20, hostLabel, connectHBox, backLabel);
        vBox.setAlignment(Pos.CENTER);
        lanVBox.setAlignment(Pos.CENTER);
        borderPane.setCenter(vBox);

        botLabel.setOnMousePressed(Menu::botMPressed);
        networkLabel.setOnMouseReleased(Menu::lanMPressed);
        hostLabel.setOnMouseReleased(Menu::hostMPressed);
        connectLabel.setOnMouseReleased(Menu::connectMPressed);
        backLabel.setOnMouseReleased(Menu::backMPressed);
        exitLabel.setOnMousePressed(Menu::exitMPressed);
        return new Scene(borderPane, Color.GREY);
    }

    private static void botMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            Main.mainStage.setScene(Game.gameScene());
        }
    }

    private static void lanMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            borderPane.getChildren().remove(vBox);
            borderPane.setCenter(lanVBox);
        }
    }


    private static void hostMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            lanVBox.getChildren().removeAll(hostLabel, connectHBox);
            Network.createConnection(true, "");
            Global.serverWaiting = 0;
            infoLabel.setText("Waiting for connection, host name : " + Network.getHostName());
            lanVBox.getChildren().add(0, infoLabel);
            pos = 1;
        }
    }

    private static void connectMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            lanVBox.getChildren().removeAll(hostLabel, connectHBox);
            Network.createConnection(false, hostNameTextField.getText());
            infoLabel.setText("Connecting to, host name : " + hostNameTextField.getText());
            lanVBox.getChildren().add(0, infoLabel);
            pos = 2;
        }
    }

    private static void backMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            if (pos == 1 || pos == 2) {
                lanVBox.getChildren().remove(infoLabel);
                lanVBox.getChildren().add(0, hostLabel);
                lanVBox.getChildren().add(1, connectHBox);
                if (pos == 1) {
                    Network.closeServerSocket();
                    Global.serverWaiting = 1;
                }
            } else {
                borderPane.getChildren().remove(lanVBox);
                borderPane.setCenter(vBox);
            }
            pos = 0;
        }
    }

    private static void exitMPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            Main.mainStage.close();
        }
    }

}
